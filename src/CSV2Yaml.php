<?php 

namespace CSV2Yaml;

use Symfony\Component\Yaml\Yaml;
use Doctrine\Common\Inflector\Inflector;


class CSV2Yaml 
{
	protected $csv;
	protected $yaml;
	protected $convert = false;
	protected $yaml_data;

	public function __construct($csv, $yaml, $convert = true)
	{
		if (!file_exists($csv)) {
			die("Não existe o arquivo {$csv}" . PHP_EOL);
		}

		if (file_exists($yaml)) {
			echo "O arquivo {$yaml} já existe. Deseja sobrescrevê-lo (S/N)? [N]".PHP_EOL;
			$sn = strtolower(trim(fread(STDIN, 1)));
			if ($sn != 's') {
				exit(0);
			}
		}
		$this->csv = $csv;
		$this->yaml = $yaml;
		$this->convert = $convert;		
	}

	public function getYaml()
	{
		$yaml_array = $this->readCSV();
		// cria a estrutura para o arquivo yml
		$this->yaml_data = Yaml::dump($yaml_array, 2, 2);

		return $this->yaml_data;
	}

	public function writeYaml()
	{
		file_put_contents($this->yaml, $this->yaml_data);
	}

	protected function readCSV()
	{
		// lê os dados do arquivo csv
		$file_csv = fopen($this->csv, 'r');
		$primeira = true;
		$cabecalho = array();
		$yaml_array = array();
		while ($linha = fgetcsv($file_csv)) {
			if ($primeira) {
				$primeira = false;
				$cabecalho = $this->getHeader($linha);
				continue;
			}
			$yaml_item = array();
			foreach ($linha as $pos => $coluna) {
				$yaml_item[$cabecalho[$pos]] = $coluna;
			}
			$yaml_array[] = $yaml_item;
		}
		return $yaml_array;
	}

	protected function getHeader($dados)
	{
		foreach ($dados as $key => $dado) {
			if ($this->convert) {
				$novo_dado = Inflector::camelize($dado);
			} else {
				$novo_dado = $dado;
			}
			$dados[$key] = $novo_dado;
		}
		return $dados;
	}

}











